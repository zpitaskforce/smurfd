#include "settings.h"
#include <QDir>

#include "submission.h"

QString Settings::mapFname(const QString& tc, QString value) {
    QRegularExpression rx("\{(.*?)}");
    auto match = rx.globalMatch(value);
    while (match.hasNext()) {
        auto i = match.next();
        QString orig = i.captured(0);
        QString repl = getFilemap(tc)[i.captured(1)];
        value.replace(orig,repl);
    }
    return value;
}

Settings::Settings() {
    QSettings global(CONF_DIR "/config.ini", QSettings::IniFormat);
    global.beginGroup("daemon");
    daemon.address = global.value("bind","127.0.0.1").toString();
    daemon.port = global.value("port",9121).toInt();
    //TODO dopisać usera i grupe dla daemona;
    global.endGroup();
    global.beginGroup("spool");
    spool.dir = global.value("dir","/tmp/smurfd/").toString();
    QDir dir(spool.dir);
    if (!dir.exists()) {
        dir.mkpath(".");
    }


    spool.max_age = global.value("max_age", 15).toInt();
    spool.interval = global.value("interval",0).toInt();
    spool.allow_drop = global.value("allow_drop",false).toBool();
    global.endGroup();

    global.beginGroup("toolchains");
    toolchains = global.allKeys();

    for (auto tc : toolchains) {
        QSettings tcs(CONF_DIR "/" + global.value(tc).toString(),QSettings::IniFormat);
        tcs.beginGroup("config");
        QSettings fmapping(CONF_DIR "/" + tcs.value("files").toString(), QSettings::IniFormat);
        fmapping.beginGroup("files");
        QMap<QString,QString> fmap;
        for (auto fids : fmapping.allKeys()) {
            fmap.insert(fids,fmapping.value(fids).toString());
        }
        filemaps.insert(tc,fmap);
        //TODO Sandbox config
        tcs.endGroup();
        int size = tcs.beginReadArray("pipeline");
        QVector<ProcessDesc> pipeline;
        for (int i = 0; i< size; i++) {
            tcs.setArrayIndex(i);
            ProcessDesc process;
            process.stage_nr = tcs.value("stage_nr",static_cast<int>(Submission::Stage::INVALID)).toInt();
            process.max_timeout = tcs.value("max_timeout",30).toInt();
            process.pexec =    mapFname(tc,tcs.value("exec").toString());
            process.pstdin =   mapFname(tc,tcs.value("stdin").toString());
            process.pstdout =  mapFname(tc,tcs.value("stdout").toString());
            process.pstderr =  mapFname(tc,tcs.value("stderr").toString());
            process.check_exit = tcs.value("check_exit",true).toBool();
            process.psandbox = tcs.value("sandbox",QString()).toString();
            pipeline.append(process);
        }
        tcs.endArray();
        pipelines.insert(tc,pipeline);
    }

}
