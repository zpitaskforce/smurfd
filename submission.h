#ifndef SUBMISSION_H
#define SUBMISSION_H

#include "settings.h"

#include "processdesc.h"

#include <QDateTime>
#include <QDebug>
#include <QReadLocker>
#include <QReadWriteLock>
#include <QTemporaryDir>
#include <QWriteLocker>

#include <unordered_map>
#include <tuple>
class Submission
{
public:

    enum class Status { NOT_STARTED = -1,
                        FIN_SUCCESS = 0,
                        FIN_ERR_CODE = 1,
                        TIMEOUT = 2,
                        CRASHED = 3,
                        RUNNING = 4 };

    enum class Stage  {  EVALUATION = 3,
                         EXECUTION = 2,
                         COMPILATION = 1,
                         ENQUEUED = 0,
                         INVALID = -1 };

    enum class Verdict { PENDING = -1,
                         FAILED = 0,
                         PASSED = 1
                       };

    const static std::unordered_map<int, QString> status_list;
    const static std::unordered_map<int, QString> stage_list;
    const static std::unordered_map<int, QString> verdict_list;
    enum class Output { STDOUT, STDERR };


    explicit Submission(QString language, QString source, QString input, QString expected, int timeout);

    QString getID() const;


    QString getFile(const QString & key) const {
        auto filemap = Settings::getInstance().getFilemap(toolchain);
        return getPath() + '/' + filemap[key];
     }

    QVector<ProcessDesc> getPipeline() const {
        return Settings::getInstance().getPipeline(toolchain);
    }


    void setStage(Stage stage) {
        QWriteLocker lock(&this->rwlock);
        this->stage = stage;
    }

    std::pair<int, QString> getStage() const {
        QReadLocker lock(&this->rwlock);
        return *stage_list.find(static_cast<int>(stage));
    }


    void setStatus(Status status) {
        QWriteLocker lock(&this->rwlock);
        this->status = status;
    }

    std::pair<int, QString> getStatus() const {
        QReadLocker lock(&this->rwlock);
        return *status_list.find(static_cast<int>(status));
    }


    std::tuple< int,int,QString> getStageAndStatus() {
        auto status = getStatus();
        auto stage = getStage();
        return std::make_tuple(stage.first, status.first, stage.second + "|" + status.second);
    }

    std::pair<int,QString> getVerdict() {
        QReadLocker lock(&this->rwlock);
        if (finished) {
            if ((stage == Stage::EVALUATION) && (status == Status::FIN_SUCCESS))
                return *verdict_list.find(static_cast<int>(Verdict::PASSED));
            return *verdict_list.find(static_cast<int>(Verdict::FAILED));
        }
        return *verdict_list.find(static_cast<int>(Verdict::PENDING));
    }

    QString getCompileInfo() const {
        return getData(Stage::COMPILATION, Output::STDERR);
    }

    QString getOutput() const {
        return getData(Stage::EXECUTION, Output::STDOUT);
    }

    QString getStderr() const {
        return getData(Stage::EXECUTION, Output::STDERR);
    }

    QString getDiff() const {
        return getData(Stage::EVALUATION, Output::STDOUT);
    }

    int getExitCode() const {
        QReadLocker lock(&this->rwlock);
        return this->lastExitCode;
    }

    void setExitCode(int exitCode) {
        QWriteLocker lock(&this->rwlock);
        this->lastExitCode = exitCode;
    }

    bool readyForCleanup() const {
        QDateTime now = QDateTime::currentDateTime();
        QReadLocker lock(&this->rwlock);
        return finished && (deathTime < now);
    }

    QString getPath() const {
        return data_root.path();
    }

    bool isFinished() const {
        QReadLocker lock(&this->rwlock);
        return finished;
    }

    void setFinished() {
        QWriteLocker lock(&this->rwlock);
        this->finished = true;
    }

    int getTimeout() const {
        return timeout;
    }

    bool allowDrop() {
        auto drop_allowed = Settings::getInstance().getSpoolSettings().allow_drop;
        if (drop_allowed) {
            QWriteLocker lock(&this->rwlock);
            this->drop = true;
        }
        return drop_allowed;
    }

private:
    Q_DISABLE_COPY(Submission)
    QString getData(const Stage stage, const Output out) const;
    void putFile(QString filename, QString content);

    Stage stage;
    Status status;
    QString toolchain;
    int timeout;
    QMap<QString, QString> config;
    QTemporaryDir data_root;
    QDateTime deathTime;

    int lastExitCode;
    bool finished;
    bool drop;
    mutable QReadWriteLock rwlock;
};

#endif // SUBMISSION_H
