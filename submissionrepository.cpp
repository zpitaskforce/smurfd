#include "submissionrepository.h"

#include "pipeline.h"

#include <QThreadPool>
#include <stdexcept>

SubmissionRepository::SubmissionRepository(QObject *parent) : QObject(parent)
{
    timer.setInterval(Settings::getInstance().getSpoolSettings().interval * 1000);
    connect(&timer,SIGNAL(timeout()),this,SLOT(cleanup()));
    if (timer.interval() > 0) {
        timer.start();
    }
}

QSharedPointer<Submission> SubmissionRepository::findSubmissinon(const QString& id)
{
    return this->repository([id](QMap<QString,QSharedPointer<Submission>> &rep) {
        return rep[id];
    });
}

QString SubmissionRepository::createSubmission(const QString &language,const QString &source, const QString &input, const QString &expected, const int timeout)
{
    shrptr ptr = shrptr::create(language,source,input,expected,timeout);
    this->repository([ptr](map_type &rep) {
        rep.insert(ptr->getID(),ptr);
    });
    Pipeline* pipeline = new Pipeline(ptr);
    QThreadPool::globalInstance()->start(pipeline);
    return ptr->getID();
}

void SubmissionRepository::deleteSubmission(const QString &id)
{
    this->repository([id](map_type &rep) {
        rep.remove(id);
    });
}

void SubmissionRepository::cleanup() {
    QList<shrptr> copy = this->repository([](map_type &rep)->QList<shrptr> {
            return rep.values();
    });
    for(auto &sub : copy) {
        if (!sub.isNull() && sub->readyForCleanup()) {
            deleteSubmission(sub->getID());
        }
    }
}

