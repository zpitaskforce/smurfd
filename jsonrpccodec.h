#ifndef JSONRPCCODEC_H
#define JSONRPCCODEC_H

#include <QJsonObject>
#include <QByteArray>
#include <QString>
#include <QVariantMap>
#include <QVariantList>
#include <exception>

class JsonRPCCodec
{
public:
    static QByteArray processRequest(const QByteArray& data,QObject* remote);
    static QVariantMap encode(const QVariant& data);
    static QVariantMap encodeException(const std::exception &e);
    static QVariantMap encodeException(const int code, const QString& msg);
private:
    static QByteArray prepareResponse(QVariantMap response,QString req_id = QString());
    static bool isValidRpcRequest(const QJsonObject& request);
    static QByteArray invokeMethod(const QString& id, const QString& method, const QVariantList& params, QObject *remote);
};

#endif // JSONRPCCODEC_H
