#ifndef PIPELINE_H
#define PIPELINE_H

#include "submission.h"
#include <QProcess>
#include <QRunnable>
#include <QSharedPointer>
#include <QVector>

class Pipeline : public QRunnable
{
public:
    Pipeline(const QSharedPointer<Submission>& task) : task(task) {}
    void run() override;

private:
    QSharedPointer<Submission> task;
};

#endif // PIPELINE_H
