#ifndef PROCESSDESC_H
#define PROCESSDESC_H
#include <QString>

struct ProcessDesc {
    int stage_nr;
    int max_timeout;
    QString pexec;
    QString pstdin;
    QString pstdout;
    QString pstderr;
    QString description;
    QString psandbox;
    bool check_exit;
};


#endif // PROCESSDESC_H
