#include "submission.h"

#include "settings.h"
#include "synchronized.h"

#include <QMutexLocker>
#include <QTextStream>
#include <algorithm>
#include <utility>

const std::unordered_map<int,QString> Submission::status_list = { std::make_pair(static_cast<int>(Submission::Status::NOT_STARTED),"Not running"),
                                                                  std::make_pair(static_cast<int>(Submission::Status::FIN_SUCCESS),"Finished succesfully"),
                                                                  std::make_pair(static_cast<int>(Submission::Status::FIN_ERR_CODE),"Finished with error code"),
                                                                  std::make_pair(static_cast<int>(Submission::Status::TIMEOUT),"Timeout"),
                                                                  std::make_pair(static_cast<int>(Submission::Status::CRASHED),"Crashed"),
                                                                  std::make_pair(static_cast<int>(Submission::Status::RUNNING),"Running")
                                                  };

const std::unordered_map<int,QString> Submission::stage_list = { std::make_pair(static_cast<int>(Submission::Stage::ENQUEUED),   "Enqueued"),
                                                                 std::make_pair(static_cast<int>(Submission::Stage::COMPILATION),"Compilation"),
                                                                 std::make_pair(static_cast<int>(Submission::Stage::EXECUTION),  "Execution"),
                                                                 std::make_pair(static_cast<int>(Submission::Stage::EVALUATION), "Evaluation"),
                                                               };

const std::unordered_map<int,QString> Submission::verdict_list = { std::make_pair(static_cast<int>(Submission::Verdict::PENDING),"Pending"),
                                                                   std::make_pair(static_cast<int>(Submission::Verdict::FAILED), "Failed"),
                                                                   std::make_pair(static_cast<int>(Submission::Verdict::PASSED), "Passed") };

Submission::Submission( QString lang, QString source,QString input, QString expected, int timeout) : stage(Stage::ENQUEUED), status(Status::NOT_STARTED)
  , toolchain(lang), timeout(timeout), data_root(Settings::getInstance().getSpoolSettings().dir), lastExitCode(1), finished(false) //,rwlock(QReadWriteLock::Recursive)
{
    deathTime = QDateTime::currentDateTime().addSecs(Settings::getInstance().getSpoolSettings().max_age*60);
    auto filemap = Settings::getInstance().getFilemap(toolchain);
    putFile(filemap.value("source"), source);
    putFile(filemap.value("input"), input);
    putFile(filemap.value("expected"),expected);
}


void Submission::putFile(QString filename, QString content) {
    QFile sourceFile(data_root.path() + "/" + filename);
    if (!sourceFile.open(QFile::WriteOnly)) {
    }
    QTextStream out(&sourceFile);
    out << content;
    sourceFile.close();
}


QString Submission::getID() const
{
    QString path = data_root.path().mid(data_root.path().lastIndexOf('/')+1);
    return path;
}

QString Submission::getData(const Stage stage, const Output out) const
{
    { //begining of critical region
    QReadLocker lock(&this->rwlock);
    if (this->stage < stage)
        //stage in question is not finished!
        return QString();
    }//end of critical region
    //stage in question is finished - it is safe to read data
    auto pipeline = this->getPipeline();
    auto index = std::find_if(pipeline.begin(),pipeline.end(),
                              [stage](const ProcessDesc &p){return p.stage_nr == static_cast<int>(stage);}
    );
    //no such stage
    if (index == pipeline.end())
        return QString();
    QString filename = this->getPath() + '/';
    switch (out) {
    case Output::STDERR: filename += index->pstderr; break;
    case Output::STDOUT: filename += index->pstdout; break;
    }
    QString result;
    QFile f(filename);
    f.open(QFile::ReadOnly);
    result = QString::fromUtf8(f.readAll());
    f.close();
    return result;
}

