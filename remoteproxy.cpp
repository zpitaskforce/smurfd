#include "remoteproxy.h"
#include "jsonrpccodec.h"
#include <QCoreApplication>
#include <stdexcept>

RemoteProxy::RemoteProxy(SubmissionRepository *parent) :
    QObject(parent),repository(parent)
{

}

QVariantMap RemoteProxy::createSubmission(const QVariantList &args) {
    //args[0] - language
    //args[1] - source code
    //args[2] - input
    //args[3] - expected output
    //args[4] - timeout [s]
    //returns: submission id

    if (!Settings::getInstance().getToolchains().contains(args.at(0).toString()))
            return JsonRPCCodec::encodeException(500, "Language not supported");

    QString id = repository->createSubmission(args.at(0).toString()
                                              ,args.at(1).toString()
                                              ,args.at(2).toString()
                                              ,args.at(3).toString()
                                              ,args.at(4).toInt());
    return JsonRPCCodec::encode(id);
}



QVariantMap RemoteProxy::getLanguages(const QVariantList &args)
{
    Q_UNUSED(args);
    auto data = Settings::getInstance().getToolchains();
    return JsonRPCCodec::encode(data);
}

QVariantMap RemoteProxy::exit(const QVariantList &args) {
    Q_UNUSED(args);
    emit quit();
    return QVariantMap();
}


QVariantMap RemoteProxy::getCompileInfo(const QVariantList &args)
{
    auto submission = repository->findSubmissinon(args.at(0).toString());
    if (!submission)
        return JsonRPCCodec::encodeException(404,"Not found");
    return JsonRPCCodec::encode(submission->getCompileInfo());
}


QVariantMap RemoteProxy::getOutput(const QVariantList &args)
{
    auto submission = repository->findSubmissinon(args.at(0).toString());
    if (!submission)
        return JsonRPCCodec::encodeException(404,"Not found");
    return JsonRPCCodec::encode(submission->getOutput());
}


QVariantMap RemoteProxy::getStderr(const QVariantList &args)
{
    auto submission = repository->findSubmissinon(args.at(0).toString());
    if (!submission)
        return JsonRPCCodec::encodeException(404,"Not found");
    return JsonRPCCodec::encode(submission->getStderr());
}


QVariantMap RemoteProxy::getDiff(const QVariantList &args)
{
    auto submission = repository->findSubmissinon(args.at(0).toString());
    if (!submission)
        return JsonRPCCodec::encodeException(404,"Not found");
    return JsonRPCCodec::encode(submission->getDiff());
}


QVariantMap RemoteProxy::getExitCode(const QVariantList &args)
{
    auto submission = repository->findSubmissinon(args.at(0).toString());
    if (!submission)
        return JsonRPCCodec::encodeException(404,"Not found");
    return JsonRPCCodec::encode(submission->getExitCode());
}

QVariantMap RemoteProxy::getStage(const QVariantList &args)
{
    auto submission = repository->findSubmissinon(args.at(0).toString());
    if (!submission)
        return JsonRPCCodec::encodeException(404,"Not found");
    auto stage = submission->getStage();
    QVariantMap stage_map;
    stage_map.insert("code",stage.first);
    stage_map.insert("description",stage.second);
    return JsonRPCCodec::encode(stage_map);
}

QVariantMap RemoteProxy::getStatus(const QVariantList &args)
{
    auto submission = repository->findSubmissinon(args.at(0).toString());
    if (!submission)
        return JsonRPCCodec::encodeException(404,"Not found");
    auto status = submission->getStatus();
    QVariantMap status_map;
    status_map.insert("code",status.first);
    status_map.insert("description",status.second);
    return JsonRPCCodec::encode(status_map);
}

QVariantMap RemoteProxy::getStageAndStatus(const QVariantList &args)
{
    auto submission = repository->findSubmissinon(args.at(0).toString());
    if (!submission)
        return JsonRPCCodec::encodeException(404,"Not found");
    auto status = submission->getStageAndStatus();
    QVariantMap status_map;
    status_map.insert("stage",std::get<0>(status));
    status_map.insert("status",std::get<1>(status));
    status_map.insert("description",std::get<2>(status));
    return JsonRPCCodec::encode(status_map);
}


QVariantMap RemoteProxy::isFinished(const QVariantList &args)
{
    auto submission = repository->findSubmissinon(args.at(0).toString());
    if (!submission)
        return JsonRPCCodec::encodeException(404,"Not found");
    return JsonRPCCodec::encode(submission->isFinished());
}

QVariantMap RemoteProxy::getVerdict(const QVariantList &args)
{
    auto submission = repository->findSubmissinon(args.at(0).toString());
    if (!submission)
        return JsonRPCCodec::encodeException(404,"Not found");
    auto verdict = submission->getVerdict();
    QVariantMap verdict_map;
    verdict_map.insert("code",verdict.first);
    verdict_map.insert("description", verdict.second);
    return JsonRPCCodec::encode(verdict_map);
}

QVariantMap RemoteProxy::getDetails(const QVariantList &args)
{
    auto submission = repository->findSubmissinon(args.at(0).toString());
    if (!submission)
        return JsonRPCCodec::encodeException(404,"Not found");
    auto cmpinfo = submission->getCompileInfo();
    auto output = submission->getOutput();
    auto stderr = submission->getStderr();
    auto diff = submission->getDiff();
    auto verdict = submission->getVerdict();
    QVariantMap all_map;
    all_map.insert("cmpinfo",cmpinfo);
    all_map.insert("output",output);
    all_map.insert("stderr",stderr);
    all_map.insert("diff",diff);
    return JsonRPCCodec::encode(all_map);
}
QVariantMap RemoteProxy::getInfo(const QVariantList &args)
{
    auto submission = repository->findSubmissinon(args.at(0).toString());
    if (!submission)
        return JsonRPCCodec::encodeException(404,"Not found");
    auto stage = submission->getStage();
    auto status = submission->getStatus();
    auto stderr = submission->getStderr();
    auto verdict = submission->getVerdict();
    QVariantMap all_map;
    all_map.insert("stage",stage.second);
    all_map.insert("status",status.second);
    all_map.insert("verdict", verdict.second);
    all_map.insert("score", (verdict.first==1)?1:0);
    return JsonRPCCodec::encode(all_map);
}

QVariantMap RemoteProxy::allowDrop(const QVariantList &args)
{
    auto submission = repository->findSubmissinon(args.at(0).toString());
    if (!submission)
        return JsonRPCCodec::encodeException(404,"Not found");
    auto ok = submission->allowDrop();
    return JsonRPCCodec::encode(ok);
}

