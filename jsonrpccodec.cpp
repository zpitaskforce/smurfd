#include "jsonrpccodec.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantList>
#include <QVariantMap>
#include <QDebug>

QByteArray JsonRPCCodec::processRequest(const QByteArray &data, QObject *remote)
{
    QJsonDocument request = QJsonDocument::fromJson(data);

    if (!request.isObject())
        return prepareResponse(encodeException(-32700,"parse error"));

    auto object = request.object();
    if (!isValidRpcRequest(object)) {
        return prepareResponse(encodeException(-32600,"invalid request"));
    }

    QString id = object.value("id").toString();
    QString method = object.value("method").toString();

    QVariantList params;
    if (object.contains("params")) {
        params = object.value("params").toVariant().toList();
    }
    return invokeMethod(id,method,params,remote);
}

QVariantMap JsonRPCCodec::encode(const QVariant & data)
{
    QVariantMap result;
    result.insert("result",data);
    return result;
}

QVariantMap JsonRPCCodec::encodeException(const std::exception &e)
{
    return encodeException(127,e.what());
}

QVariantMap JsonRPCCodec::encodeException(const int code, const QString &msg) {
    QVariantMap result;
    QVariantMap error;
    error.insert("code",code);
    error.insert("message",msg);
    result.insert("error",error);
    return result;
}


QByteArray JsonRPCCodec::prepareResponse(QVariantMap response, QString req_id)
{
    response.insert("jsonrpc","2.0");
    if (req_id.isEmpty())
        response.insert("id",QVariant());
    else
        response.insert("id",req_id);
    return QJsonDocument::fromVariant(response).toJson();
}


bool JsonRPCCodec::isValidRpcRequest(const QJsonObject &request)
{
    if ((!request.contains("jsonrpc")) || (request.value("jsonrpc").toString() != "2.0"))
        return false;
    if (!request.contains("id"))
        return false;
    if (!request.contains("method"))
        return false;
    return true;
}

QByteArray JsonRPCCodec::invokeMethod(const QString &id, const QString &method, const QVariantList &params, QObject * remote)
{
    QVariantMap response;
    if (!QMetaObject::invokeMethod(remote,method.toLatin1().constData(),
                              Qt::DirectConnection,
                              Q_RETURN_ARG(QVariantMap,response),
                              Q_ARG(QVariantList,params))) {
        return prepareResponse(encodeException(-32601,"method not found"));
    }
    return prepareResponse(response,id);
}

