#ifndef SANDBOXEDPROCESS_H
#define SANDBOXEDPROCESS_H

#include "timeoutprocess.h"

class SandboxedProcess : public TimeoutProcess
{
public:
    SandboxedProcess(QString sandbox_lib, int secs, QObject* parent = nullptr):
            TimeoutProcess(secs,parent), sandbox_lib(sandbox_lib) {}
private:
    QString sandbox_lib;
protected:
    void setupChildProcess() override;
};

#endif // SANDBOXEDPROCESS_H
