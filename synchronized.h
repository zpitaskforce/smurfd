#ifndef SYNCHRONIZED_H
#define SYNCHRONIZED_H

#include <QMutex>
#include <QMutexLocker>

template <typename T>
class Synchronized
{
private:
    mutable QMutex _m;
    mutable T t;

public:
    Synchronized() {}
    Synchronized(T t) : t(t)
    {}

    template <typename F>
    auto operator()(F f) const -> decltype(f(t)) {
        QMutexLocker(&this->_m);
        return f(t);
    }
};

#endif // SYNCHRONIZED_H
