#ifndef SMURFDSERVER_H
#define SMURFDSERVER_H

#include "remoteproxy.h"

#include <QCoreApplication>
#include <QSocketNotifier>
#include <QTcpServer>
#include <iostream>
#include <unistd.h>

class SmurfdServer : public QTcpServer
{
    Q_OBJECT
public:
    explicit SmurfdServer(QObject *parent = 0);
    static void unixSigTermHandler(int);
protected:
    void incomingConnection(const qintptr socket_desc);
public slots:
    void handleTermSignal();
private:
    RemoteProxy* remote;
    QSocketNotifier* sigtermNotifier;
    static int fd_term[2];
};

#endif // SMURFDSERVER_H
