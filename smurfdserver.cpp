#include "smurfdserver.h"

#include "clientconnection.h"
#include "remoteproxy.h"
#include "submissionrepository.h"

#include <QCoreApplication>
#include <sys/socket.h>

int SmurfdServer::fd_term[2];

void SmurfdServer::unixSigTermHandler(int) {
    char x = 1;
    ::write(fd_term[0],&x,sizeof(x));
}

void SmurfdServer::handleTermSignal() {
    sigtermNotifier->setEnabled(false);
    char x;
    ::read(fd_term[1],&x,sizeof(char));
    sigtermNotifier->setEnabled(true);
    QMetaObject::invokeMethod(QCoreApplication::instance(),"quit",Qt::QueuedConnection);
}

SmurfdServer::SmurfdServer(QObject *parent) :
    QTcpServer(parent)
{
    if(::socketpair(AF_UNIX, SOCK_STREAM,0,fd_term))
        qFatal("Couldn't create TERM socket");
    sigtermNotifier = new QSocketNotifier(fd_term[1], QSocketNotifier::Read, this);
    connect(sigtermNotifier,SIGNAL(activated(int)),this,SLOT(handleTermSignal()));


    SubmissionRepository *repository = new SubmissionRepository(this);
    this->remote = new RemoteProxy(repository);
    connect(this->remote,SIGNAL(quit()),QCoreApplication::instance(),SLOT(quit()));
}

void SmurfdServer::incomingConnection(const qintptr socket_desc)
{
    QThread *clientThread = new QThread(this);
    connect(this->remote,SIGNAL(quit()),clientThread,SLOT(terminate()));
    ClientConnection *connection = new ClientConnection(socket_desc, this->remote);
    connection->moveToThread(clientThread);
    connect(clientThread,SIGNAL(started()),connection,SLOT(run()));
    connect(clientThread,SIGNAL(finished()),connection,SLOT(deleteLater()));
    connect(clientThread,SIGNAL(finished()),clientThread,SLOT(deleteLater()));
    clientThread->start();
}

