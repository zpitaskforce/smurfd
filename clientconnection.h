#ifndef CLIENTCONNECTION_H
#define CLIENTCONNECTION_H

#include <QTcpSocket>
#include <QThread>
#include <QVariantMap>

class ClientConnection : public QObject
{
    Q_OBJECT
public:
    explicit ClientConnection(const qintptr socket_desc, QObject *remote, QObject *parent = 0);
signals:

public slots:
    void processRequest();
    void run();
private:
   qintptr socket_desc;
   QTcpSocket *socket;
   QObject* remote;
};

#endif // CLIENTCONNECTION_H
