#ifndef SUBMISSIONREPOSITORY_H
#define SUBMISSIONREPOSITORY_H

#include "submission.h"
#include "synchronized.h"

#include <QMap>
#include <QSharedPointer>
#include <QThread>
#include <QTimer>


class SubmissionRepository : public QObject
{
    typedef QSharedPointer<Submission> shrptr;
    typedef QWeakPointer<Submission> weakptr;
    typedef QMap<QString,shrptr> map_type;
    Q_OBJECT
public:
    SubmissionRepository(QObject *parent = 0);
private:

public slots:
    shrptr findSubmissinon(const QString &id);
    QString createSubmission(const QString &source, const QString & language, const  QString & input, const QString & expected, const int timeout);
    void deleteSubmission(const QString& id);
    void cleanup();
private:
    Synchronized<map_type> repository;
    QTimer timer;
};

#endif // SUBMISSIONREPOSITORY_H
