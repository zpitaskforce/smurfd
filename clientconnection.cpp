#include "clientconnection.h"

#include "jsonrpccodec.h"

#include <QByteArray>
#include <QDataStream>
#include <QTcpSocket>

ClientConnection::ClientConnection(const qintptr socket_desc, QObject* remote, QObject *parent) :
    QObject(parent),socket_desc(socket_desc),remote(remote)
{
}

void ClientConnection::run()
{
    socket = new QTcpSocket(this);
    connect(socket,SIGNAL(readyRead()),this,SLOT(processRequest()));
    connect(socket,SIGNAL(disconnected()),this->thread(),SLOT(quit()));
    if (!socket->setSocketDescriptor(this->socket_desc)) {
        return;
    }
}

void ClientConnection::processRequest()
{
    while (socket->bytesAvailable() < 20) {
        if (!socket->waitForReadyRead()) {
            socket->disconnectFromHost();
            return;
        }
    }
    auto input = socket->readLine(20);
    input.chop(1);
    long long size = input.toLongLong();

    while (socket->bytesAvailable() < size)
    {
        if(!socket->waitForReadyRead()) {
            socket->disconnectFromHost();
            return;
        }
    }
    QByteArray data = socket->read(size);
    socket->write(JsonRPCCodec::processRequest(data,remote));
    socket->disconnectFromHost();

}





