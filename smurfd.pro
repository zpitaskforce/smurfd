#-------------------------------------------------
#
# Project created by QtCreator 2013-11-15T21:53:44
#
#-------------------------------------------------


QMAKE_CXXFLAGS += -std=c++11
QMAKE_CXXFLAGS_RELEASE -= -march=x86-64 -mtune=generic
QMAKE_CXXFLAGS_RELEASE += -march=native -mtune=native
#SANITIZE = #-fsanitize=address-full
QMAKE_CXXFLAGS_DEBUG += $$SANITIZE -O0 -DNODAEMON
QMAKE_LFLAGS_DEBUG += $$SANITIZE
LIBS += -ldl

QT       += core network

QT       -= gui

TARGET = smurfd
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    smurfdserver.cpp \
    clientconnection.cpp \
    submission.cpp \
    submissionrepository.cpp \
    pipeline.cpp \
    remoteproxy.cpp \
    settings.cpp \
    jsonrpccodec.cpp \
    timeoutprocess.cpp \
    sandboxedprocess.cpp

HEADERS += \
    smurfdserver.h \
    clientconnection.h \
    synchronized.h \
    submissionrepository.h \
    pipeline.h \
    remoteproxy.h \
    processdesc.h \
    jsonrpccodec.h \
    timeoutprocess.h \
    sandboxedprocess.h

isEmpty(PREFIX) {
    PREFIX = /usr
}

config.files = ./etc/smurfd/*
config.path = /etc/$$TARGET

service.files = ./etc/init.d/*
service.path =  /etc/init.d

target.path = $$PREFIX/bin
INSTALLS += target config service
