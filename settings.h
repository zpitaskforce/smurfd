#ifndef SETTINGS_H
#define SETTINGS_H


#include <QDebug>
#include <QMap>
#include <QRegularExpression>
#include <QSettings>
#include <QStringList>

#include "processdesc.h"

#ifndef CONF_DIR
#define CONF_DIR "/etc/smurfd"
#endif

struct SpoolSettings {
    QString dir;
    int max_age;
    int interval;
    bool allow_drop;
};

struct DaemonSettings {
    QString address;
    int port;
    QString user;
    QString group;
};

class Settings
{
public:
    static Settings& getInstance() {
        static Settings settings;
        return settings;
    }

    QVector<ProcessDesc> getPipeline(const QString& toolchain) const {
        return this->pipelines[toolchain];
    }

    QMap<QString,QString> getFilemap(const QString& toolchain) const {
        return this->filemaps[toolchain];
    }

    QStringList getToolchains() const {
        return this->toolchains;
    }

    SpoolSettings getSpoolSettings() const {
        return this->spool;
    }

    DaemonSettings getDaemonSettings() const {
        return this->daemon;
    }

private:
    QString mapFname(const QString &tc, QString value);
    Settings();
    QStringList toolchains;
    QMap<QString,QMap<QString,QString>> filemaps;
    QMap<QString,QVector<ProcessDesc>> pipelines;
    SpoolSettings spool;
    DaemonSettings daemon;
};

#endif // SETTINGS_H
