#include "pipeline.h"
#include "processdesc.h"
#include "timeoutprocess.h"


void Pipeline::run()
{
    QVector<ProcessDesc> pipeline;
    pipeline = task->getPipeline();
    for (auto pd : pipeline) {
        qDebug() << pd.pexec;
        task->setStatus(Submission::Status::NOT_STARTED);
        auto p = TimeoutProcess::createSandboxedProcess(pd,*task);
        if (pd.stage_nr != static_cast<int>(Submission::Stage::INVALID))
            task->setStage(static_cast<Submission::Stage>(pd.stage_nr));
        p->start(pd.pexec);
        p->waitForStarted();
        task->setStatus(Submission::Status::RUNNING);
        bool timeout = p->waitForTimeout();
        if(timeout) {
            task->setStatus(Submission::Status::TIMEOUT);
            break;
        }
        if (p->exitStatus() != QProcess::NormalExit) {
            task->setStatus(Submission::Status::CRASHED);
            break;
        }
        if (p->exitCode() != 0) {
            task->setStatus(Submission::Status::FIN_ERR_CODE);
            task->setExitCode(p->exitCode());
            if (pd.check_exit)
                break;
        }
        task->setStatus(Submission::Status::FIN_SUCCESS);
        task->setExitCode(0);
    }
    this->task->setFinished();
}


