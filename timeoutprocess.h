#ifndef TIMEOUTPROCESS_H
#define TIMEOUTPROCESS_H

#include <QProcess>
#include <memory>

struct ProcessDesc;
class Submission;

class TimeoutProcess : public QProcess
{
public:
    static std::unique_ptr<TimeoutProcess> createSandboxedProcess(const ProcessDesc &pd, const Submission &task);
    TimeoutProcess(int secs,QObject *parent = nullptr) : QProcess(parent), timeout_msecs(secs*1000) {}
    bool waitForTimeout();

private:
    int timeout_msecs;
};

#endif // TIMEOUTPROCESS_H
