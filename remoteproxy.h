#ifndef REMOTEPROXY_H
#define REMOTEPROXY_H
#include "submissionrepository.h"
#include <QObject>
#include <QString>
#include <QVariant>
#include <QVariantList>
#include <QVariantMap>

#include "submission.h"

class RemoteProxy : public QObject
{
    Q_OBJECT
public:
    explicit RemoteProxy(SubmissionRepository *parent);

signals:
    void quit();
public slots:
    QVariantMap createSubmission(const QVariantList& args);
    QVariantMap getLanguages(const QVariantList& args);
    QVariantMap exit(const QVariantList& args);
    QVariantMap getCompileInfo(const QVariantList &args);
    QVariantMap getOutput(const QVariantList &args);
    QVariantMap getStderr(const QVariantList &args);
    QVariantMap getDiff(const QVariantList &args);
    QVariantMap getExitCode(const QVariantList &args);
    QVariantMap getStage(const QVariantList &args);
    QVariantMap getStatus(const QVariantList &args);
    QVariantMap getStageAndStatus(const QVariantList &args);
    QVariantMap isFinished(const QVariantList &args);
    QVariantMap getVerdict(const QVariantList &args);
    QVariantMap getDetails(const QVariantList &args);
    QVariantMap getInfo(const QVariantList &args);
    QVariantMap allowDrop(const QVariantList &args);
private:
    SubmissionRepository *repository;
};


#endif // REMOTEPROXY_H
