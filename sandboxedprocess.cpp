#include "sandboxedprocess.h"
#include <QDebug>
#include <dlfcn.h>
#include <memory>
#include <iostream>


void SandboxedProcess::setupChildProcess()
{
    typedef void (*sbfun_t)();
    std::string libpath = this->sandbox_lib.toStdString();
    std::unique_ptr<void> handle(dlopen(libpath.data(),RTLD_LAZY));
    if (!handle)
    {
        std::cerr << "Could not load sandbox library";
        kill();
    }
    dlerror();

    sbfun_t sandbox = (sbfun_t) dlsym(handle.get(),"sandbox");

    const char *error = dlerror();

    if (error || !sandbox)
    {
        std::cerr << "Could not load sandboxing function" << dlerror();
        kill();
    }

    sandbox();

}
