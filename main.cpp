
#include "settings.h"
#include <QCoreApplication>
#include <signal.h>
#include <unistd.h>

#include "smurfdserver.h"
#include <QDebug>

static void setup_unix_signal_handlers() {
    struct sigaction term;
    term.sa_handler = SmurfdServer::unixSigTermHandler;
    sigemptyset(&term.sa_mask);
    term.sa_flags |= SA_RESTART;
    sigaction(SIGTERM, &term, 0);
}

int main(int argc, char *argv[])
{
    qDebug() << " Kompilacja: " << __DATE__ <<  __TIME__;
    QCoreApplication a(argc, argv);
    SmurfdServer server;
    setup_unix_signal_handlers();
    if (!server.listen(QHostAddress(
                           Settings::getInstance().getDaemonSettings().address)
                       ,Settings::getInstance().getDaemonSettings().port))
    {
        qDebug() << "Could not bind to: " << Settings::getInstance().getDaemonSettings().address
                 << ":" << Settings::getInstance().getDaemonSettings().port;
        return 1;
    }
    daemon(0,0);
    return a.exec();
}
