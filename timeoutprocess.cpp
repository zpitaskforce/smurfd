#include "timeoutprocess.h"
#include "processdesc.h"
#include "timeoutprocess.h"
#include "sandboxedprocess.h"
#include "submission.h"

std::unique_ptr<TimeoutProcess> TimeoutProcess::createSandboxedProcess(const ProcessDesc& pd, const Submission &task)
{
    int timeout = pd.max_timeout;
    if (pd.stage_nr == static_cast<int>(Submission::Stage::EXECUTION) &&  task.getTimeout() > 0 && task.getTimeout() < timeout) {
        timeout = task.getTimeout();
    }
    std::unique_ptr<TimeoutProcess> ptr;

    if (pd.psandbox.isEmpty()) {
        ptr.reset(new TimeoutProcess(timeout));
    }
    else {
        ptr.reset(new SandboxedProcess(pd.psandbox,timeout));
    }
    ptr->setWorkingDirectory(task.getPath());
    if (!pd.pstdin.isEmpty()) {
        ptr->setStandardInputFile(task.getPath()  + "/" + pd.pstdin);
    }
    if (!pd.pstdout.isEmpty()) {
        ptr->setStandardOutputFile(task.getPath()  + "/" + pd.pstdout);
    }
    if (!pd.pstderr.isEmpty()) {
        ptr->setStandardErrorFile(task.getPath()  + "/" + pd.pstderr);
    }
    return ptr;
}

bool TimeoutProcess::waitForTimeout()
{
    bool isFinished = waitForFinished(timeout_msecs);
    if (!isFinished && state() == QProcess::Running) {
        kill();
        waitForFinished();
        return true;
    }
    return false;
}

